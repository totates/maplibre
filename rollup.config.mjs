import commonjs from "@rollup/plugin-commonjs";
import fs from "fs-extra";
import path from "path";
import replace from "@rollup/plugin-replace";
import resolve from "@rollup/plugin-node-resolve";
import terser from "@rollup/plugin-terser";
import { Component } from "totates";
import { createHash } from "crypto";

import data from "./component.json" assert { type: "json" };
const component = new Component(data);
const { destDir, manifest } = component;

const maplibreDir = "node_modules/maplibre-gl";

const icons = [
    {
        name: "icon.white",
        file: "white.webp",
        type: "image/webp",
        base64: "UklGRh4AAABXRUJQVlA4TBEAAAAvAQAAAAfQ//73v/+BiOh/AAA="
    },
    {
        name: "icon.dot",
        file: "dot.png",
        type: "image/png",
        base64: (
            "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAA" +
            "AAC0lEQVQYV2NgAAIAAAUAAarVyFEAAAAASUVORK5CYII="
        )
    }
];

for (const icon of icons) {
    icon.path = `${manifest.baseUrl}/images/${icon.file}`;
    icon.url = `data:${icon.type};base64,${icon.base64}`;
}

async function buildStart(options) {

    for (const icon of icons) {
        const content = Buffer.from(icon.base64, "base64");
        await manifest.addResourceFromContent(
            icon.name, content, icon.type, destDir, `images/${icon.file}`
        );
    }

    const cssInputFile = path.join(maplibreDir, "dist/maplibre-gl.css");
    const rawCss = await fs.readFile(cssInputFile, "utf8");

    const images = {};
    function replacer(match, p1, offset, string) {
        const svg = unescape(p1);
        const hash = createHash("md5");
        hash.update(svg);
        const md5sum = hash.digest("hex");
        // TODO: take only a few characters from the md5sum, enough to be unique
        const url = `images/${md5sum}.svg`;
        images[url] = svg;
        return `${manifest.baseUrl}/${url}`;
    }
    const pattern = /"data:image\/svg\+xml;charset=utf-8,([^"]+)"/gu;
    const processedCss = rawCss.replace(pattern, replacer);

    await fs.ensureDir(path.join(destDir, "images"));
    for (const [url, svg] of Object.entries(images)) {
        const resourceName = path.basename(url, ".svg").replace("/", ".");
        await manifest.addResourceFromContent(
            resourceName, svg, "image/svg+xml", destDir, url
        );
    }
    await manifest.addResourceFromContent(
        "upstream.style", processedCss, "text/css", destDir, "maplibre-gl.css"
    );
}

export default [
    {
        // TODO: use esm module when it is provided upstream
        input: "maplibre-gl.js",
        output: {
            file: path.join(destDir, "maplibre-gl.js"),
            format: "esm"
        },
        plugins: [
            replace({
                [icons[0].url]: icons[0].path,
                [icons[1].url]: icons[1].path,
                delimiters: ["", ""],
                preventAssignment: true
            }),
            commonjs(),
            resolve({ browser: true }),
            component.resource("upstream.script", "application/javascript"),
            process.env.BUILD === "production" && terser()
        ]
    },
    {
        input: "base.js",
        output: {
            file: path.join(destDir, "base.js"),
            format: "esm"
        },
        makeAbsoluteExternalsRelative: false,
        plugins: [
            {
                name: "maplibre",
                buildStart
            },
            resolve({ browser: true }),
            component.resolver(),
            component.resource("base", "application/javascript"),
            process.env.BUILD === "production" && terser()
        ]
    },
    ...component.getConfig()
];
