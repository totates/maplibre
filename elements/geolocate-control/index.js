import { GeolocateControl } from "external:@totates/maplibre-gl/v1:upstream.script";

import { tagName } from "./index.json";

class GeolocateControlElement extends HTMLElement {
    async setMapElement(el) {
        const options = {};
        /*
          options.positionOptions  {enableHighAccuracy:false,timeout:6000}
          options.fitBoundsOptions  {maxZoom:15}
          options.trackUserLocation  false
          options.showAccuracyCircle  true
          options.showUserLocation  true
        */
        const position = this.getAttribute("position");
        if (position)
            options.position = position;
        const control = new GeolocateControl(options);
        // control.addTo(el.getMap());
        const map = el.getMap();
        map.addControl(control);
    }
}

window.customElements.define(tagName, GeolocateControlElement);
