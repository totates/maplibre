import logging from "external:@totates/logging/v1";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";
import StylePlugin from "external:@totates/quotquot/v1:plugin.style";
import orientation from "external:@totates/orientation/v1";
import geolocation from "external:@totates/geolocation/v1";
import zip from "external:@totates/lodash/v4:zip";
import { Map } from "external:@totates/maplibre-gl/v1:upstream.script";

import { tagName } from "./index.json";
import resources from "./resources.js";

const logger = logging.getLogger(`@totates/maplibre-gl/v1:element.${tagName}`);
logger.setLevel("DEBUG");

const VISIBILITY = "visibility";
const VISIBLE = "visible";
const NONE = "none";

function wildcardToRegExp(s) {
    /* https://gist.github.com/donmccurdy/6d073ce2c6f3951312dfa45da14a420f */
    const pat = s.split(/\*+/u).map(p => p.replace(/[|\\{}()[\]^$+*?.]/gu, "\\$&")).join(".*");
    return new RegExp(pat, "u");
}

const styles = Object.values(resources["@totates/maplibre-gl/v1"]);

class MaplibreMapHTMLElement extends QuotquotElement {

    #map;
    #layers;

    constructor() {
        super({ state: { styles }, Plugins: [StylePlugin], plugins: [
            {
                name: "maplibre",
                requires: ["shadow"],
                setup: async(el, plugins) => this._init(plugins.shadow.root).catch(logger.error),
                cleanup: async(el, plugins)  => {},
                start: async(el, plugins)  => {},
                stop: async(el, plugins)  => {}
            }
        ]});
        this.#layers = new window.Map();
    }

    async _init(shadowRoot) {
        const div = document.createElement("div");
        div.className = "map";
        shadowRoot.appendChild(div);

        await Promise.all([geolocation.initialized, orientation.initialized]);
        const position = geolocation.getPosition() || await geolocation.loadPosition();
        const center = position ? [position.coords.longitude, position.coords.latitude] : [0, 0];
        this.#map = new Map({ center, zoom: 20, container: div });
        this.#map.on("styleimagemissing", e => logger.warn("missing image", e.id));
        // this.#map.showCollisionBoxes = true;

        const slot = document.createElement("slot");
        slot.setAttribute("name", "maplet");
        slot.addEventListener(
            "slotchange",
            e => this.initElements(slot.assignedElements()).catch(logger.error)
        );
        shadowRoot.appendChild(slot);

        /** @todo the tasks should be parallelized (which requires
         * a kind of dependency graph) */
        // const intervalId = geolocation.moveRandomly(10);
        // clearInterval(intervalId);
    }

    async initElements(elements) {
        const promises = elements.map(el => el.setMapElement(this));
        const results = await Promise.allSettled(promises);
        for (const [el, { status, reason }] of zip(elements, results)) {
            const name = el.tagName.toLowerCase();
            switch (status) {
                case "fulfilled":
                    logger.debug("init:", name, "OK");
                    break;
                case "rejected":
                    logger.warn("init:", name, reason);
                    break;
                default:
                    logger.error("unexpected allSettled status", status);
            }
        }
    }

    addControl(control, position) {
        return this.#map.addControl(control, position);
    }

    removeControl(control) {
        return this.#map.removeControl(control);
    }

    project(lnglat) {
        return this.#map.project(lnglat);
    }

    on(type, layer, listener) {
        return this.#map.on(type, layer, listener);
    }

    off(type, layer, listener) {
        return this.#map.off(type, layer, listener);
    }

    once(type, layer, listener) {
        return this.#map.once(type, layer, listener);
    }

    get map() {
        return this.#map;
    }

    get center() {
        return this.#map.getCenter();
    }

    get cursor() {
        const canvas = this.#map.getCanvasContainer();
        return canvas.style.cursor;
    }

    set cursor(cursor) {
        const canvas = this.#map.getCanvasContainer();
        canvas.style.cursor = cursor;
    }

    get layers() {
        return Array.from(this.#layers.keys()); //this.#map.getStyle().layers;
    }

    addLayer(layer, priority = 0) {
        let beforeId;
        let cur;
        for (const [l,p] of this.#layers.entries()) {
            if (priority <= p && (!cur || p <= cur)) {
                cur = p;
                beforeId = l.id;
            }
        }
        const res = this.#map.addLayer(layer, beforeId);
        this.#layers.set(layer, priority);
        //logger.debug("added layer", layer.id, "with priority", priority, "before", beforeId);
        const event = new window.CustomEvent("layer-added", { detail: { layer, priority }});
        this.dispatchEvent(event);
        return res;
    }

    removeLayer(layerId) {
        this.#map.removeLayer(layerId);
        const event = new window.CustomEvent("layer-removed", { detail: layerId });
        this.dispatchEvent(event);
    }

    setMaxBounds(bounds) {
        this.#map.setMaxBounds(bounds);
        logger.debug("bounds set to", bounds);
    }

    setStyle(style) { /** @return Promise */
        const styleLoaded = new Promise((resolve, reject) => {
            // TODO: protect with lock to be sure error is about style
            this.#map.on("error", reject);
            this.#map.on("styledata", resolve); /** < @todo ok to resolve on first? */
        });
        this.#map.setStyle(style);
        return styleLoaded;
    }

    createSourceDataPromise(sourceName) {
        return new Promise((resolve, reject) => {
            const unsubscribe = () => {
                this.#map.off("error", error);
                this.#map.off("sourcedata", sourcedata);
            };
            const error = err => {
                logger.error(err);
                if (true /** @todo parse string to find out of it is about our source */) {
                    unsubscribe();
                    reject(err);
                }
            };
            const sourcedata = e => {
                if (e.sourceId === sourceName && e.isSourceLoaded) {
                    // logger.debug("source", sourceName, "loaded");
                    unsubscribe();
                    resolve(e);
                }
            };
            this.#map.on("error", error);
            this.#map.on("sourcedata", sourcedata);
        });
    }

    async addSourceElement(el, center) {
        /** @todo use source events */
        el.setMap(this.#map);
        const sourceLoaded = this.createSourceDataPromise(el.name);
        const source = await el.getSource();
        this.#map.addSource(el.name, source);
        await sourceLoaded;
    }

    addFeatureCollection(sourceName, data, attribution) {
        const source = { type: "geojson", data };
        if (attribution)
            source.attribution = attribution;
        // const sourceLoaded = this.createSourceDataPromise(sourceName);
        this.#map.addSource(sourceName, source);
        // return sourceLoaded;
    }

    updateFeatureCollection(sourceName, data) {
        // const sourceLoaded = this.createSourceDataPromise(sourceName);
        const source = this.#map.getSource(sourceName);
        source.setData(data);
        // return sourceLoaded;
    }

    isVisible(layerId) {
        return this.#map.getLayoutProperty(layerId, VISIBILITY) === VISIBLE;
    }

    setVisibility(visibility) {
        if (VISIBLE in visibility) {
            visibility.visible.forEach(
                layer => this.#map.setLayoutProperty(layer.id, VISIBILITY, VISIBLE)
            );
        }
        if (NONE in visibility) {
            visibility.none.forEach(
                layer => this.#map.setLayoutProperty(layer.id, VISIBILITY, NONE)
            );
        }
        // this.emit(VISIBILITY, visibility);
    }

    select(pattern) {
        const regexp = wildcardToRegExp(pattern);
        return this.getLayers().filter(layer => regexp.test(layer.id));
    }

    showLayers(pattern) {
        const layers = this.select(pattern);
        this.setVisibility({ visible: layers });
        return layers;
    }

}

window.customElements.define(tagName, MaplibreMapHTMLElement);
