import { NavigationControl } from "external:@totates/maplibre-gl/v1:upstream.script";
import { tagName } from "./index.json";

class NavigationControlElement extends HTMLElement {
    async setMapElement(el) {
        const options = {};
        const position = this.getAttribute("position");
        if (position)
            options.position = position;
        const control = new NavigationControl(options);
        // control.addTo(el.getMap());
        el.addControl(control);
    }
}

window.customElements.define(tagName, NavigationControlElement);
