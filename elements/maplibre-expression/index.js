import { MaplibreLayerBaseElement } from "external:@totates/maplibre-gl/v1:base";
import { tagName } from "./index.json";

class MaplibreExpressionElement extends MaplibreLayerBaseElement {
    static get attributeTypes() {
        return {
        };
    }
}

window.customElements.define(tagName, MaplibreExpressionElement);
