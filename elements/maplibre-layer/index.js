import { MaplibreLayerBaseElement } from "external:@totates/maplibre-gl/v1:base";
import { tagName } from "./index.json";

class MaplibreLayerElement extends MaplibreLayerBaseElement {
    constructor() {
        super();
        ["filter", "layout", "paint"].forEach(name => {
            const slot = document.createElement("slot");
            slot.setAttribute("name", name);
            this.shadowRoot.appendChild(slot);
        });
    }

    static get attributeTypes() {
        return {
            id: "str",
            type: "str",
            minzoom: "number",
            maxzoom: "number"
        };
    }

    async getData() {
        const data = await super.getData();
        for (const name of ["filter", "layout", "paint"]) {
            const slot = this.$(`slot[name="${name}"]`);
            const slotted = slot.assignedElements();
            if (slotted.length)
                data[name] = await slotted[0].getData(); /** @todo others? */
        }
        console.log(JSON.stringify(data));
        return data;
    }
}

window.customElements.define(tagName, MaplibreLayerElement);
