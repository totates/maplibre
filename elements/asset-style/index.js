import logging from "external:@totates/logging/v1";
import { tagName } from "./index.json";

const logger = logging.getLogger(`@totates/maplibre-gl/v1:element.${tagName}`);

class AssetStyleElement extends HTMLElement {
    async setMapElement(el) {
        const styleAssetName = this.getAttribute("style-asset") ||
              "element-maplibre:maplibre.quotquot";
        const url = await getResource(...styleAssetName.split(":"));
        const response = await window.fetch(url);
        if (!response.ok) throw new Error(`could not fetch ${url}`);
        const style = await response.json();
        logger.debug("style", url, "loaded");
        await el.setStyle(style);
    }
}

window.customElements.define(tagName, AssetStyleElement);
