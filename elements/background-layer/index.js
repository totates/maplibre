import { tagName } from "./index.json";

class BackgroundLayerElement extends HTMLElement {
    async setMapElement(el) {
        el.getMap().addLayer({
            id: this.dataset.id,
            type: "background",
            paint: {
                "background-color": this.dataset.backgroundColor
            }
        });
    }
}

window.customElements.define(tagName, BackgroundLayerElement);
