import { MaplibreLayerBaseElement } from "external:@totates/maplibre-gl/v1:base";

import { tagName } from "./index.json";

class MaplibreLayoutElement extends MaplibreLayerBaseElement {
    static get attributeTypes() {
        return {
            visibility: "str",
            "line-join": "str",
            "line-cap": "str"
        };
    }
}

window.customElements.define(tagName, MaplibreLayoutElement);
