import logging from "external:@totates/logging/v1";
import orientation from "external:@totates/orientation/v1";
import geolocation from "external:@totates/geolocation/v1";
// TODO: use turf or maplibre itself
import { toRadians } from "ol/math.js";
import { offset } from "ol/sphere.js";

import { tagName } from "./index.json";

const logger = logging.getLogger(`@totates/maplibre-gl/v1:element.${tagName}`);
logger.setLevel("DEBUG");

class CameraMapletElement extends HTMLElement {
    constructor() {
        super();
        // TODO: move to init, use pref
        /* Using 40cm for circle representing user, matching shoulder width
         * https://www.healthline.com/health/average-shoulder-width */
        this.frustum = { near: 0.4, far: 5, aspect: 1.0, fov: Math.PI / 5 };
        this.euler = { alpha: 0 };
    }

    getFeatureCollection() {
        /* const center = turf.point(this.lonlat);
           const radius = .005; // in km
           const bearing1 = 25;
           const bearing2 = 45;
           const sector = turf.sector(center, radius, bearing1, bearing2);*/
        this.accuracy = 5;

        const apex = [];
        for (let t = -Math.PI; t <= Math.PI; t += 0.05)
            apex.push(offset(this.lonlat, this.frustum.near, t));

        const halo = [];
        for (let t = -Math.PI; t <= Math.PI; t += 0.05)
            halo.push(offset(this.lonlat, this.accuracy, t));

        const frustum = [];
        const halfhfov = this.frustum.aspect * this.frustum.fov / 2;
        const bisector = -this.euler.alpha + toRadians(screen.orientation.angle);
        for (let t = bisector - halfhfov; t <= bisector + halfhfov; t += 0.05) {
            frustum.unshift(offset(this.lonlat, this.frustum.near, t));
            frustum.push(offset(this.lonlat, this.frustum.far, t));
        }

        // { units: 'kilometers', steps: 64, properties: {} }
        // const apex = turf.sector(center, radius, bearing1, bearing2, );*/

        return {
            type: "FeatureCollection",
            features:[
                {
                    type: "Feature",
                    geometry: {
                        type: "Polygon",
                        coordinates: [apex]
                    },
                    properties: { name: "apex" }
                },
                {
                    type: "Feature",
                    geometry: {
                        type: "Polygon",
                        coordinates: [halo]
                    },
                    properties: { name: "halo" }
                },
                {
                    type: "Feature",
                    geometry: {
                        type: "Polygon",
                        coordinates: [frustum]
                    },
                    properties: { name: "frustum" }
                }
            ]
        };
    }

    async setMapElement(el) {
        const { lng, lat } = el.center;
        this.lonlat = [lng, lat];
        const sourceName = "camera";// this.getAttribute('source');

        await el.addFeatureCollection(sourceName, this.getFeatureCollection());

        /*
        el.addEventListener("layer-added", e => logger.debug("layer added", e.detail.layer.id));
        el.addEventListener("layer-removed", e => logger.debug("layer removed", e.detail));
        */

        this.draw = () => el.updateFeatureCollection(sourceName, this.getFeatureCollection());
        // el.easeTo({ center: this.lonlat });
        // el.easeTo({ bearing: toDegrees(-this.euler.alpha) });

        geolocation.on("position", position => {
            this.lonlat = [position.coords.longitude, position.coords.latitude];
            this.accuracy = position.accuracy;
            this.draw();
        });
        geolocation.enableTracking(true);

        orientation.on("orientation", _orientation => {
            this.euler = _orientation.euler;
            this.draw();
        });
        orientation.enableReading(10).catch(logger.error);
        //await orientation.enableReading(10);

        // window.addEventListener('orientationchange', () => this.draw(), false);

        const layers = {
            apex: {
                paint: {
                    "fill-color": "rgba(0, 255, 255, 0.5)",
                    "fill-outline-color": "#088"
                }
            },
            halo: {
                paint: {
                    "fill-color": "rgba(0, 0, 255, 0.1)",
                    "fill-outline-color": "blue"
                }
            },
            frustum: {
                paint: {
                    "fill-color": "rgba(0, 255, 0, .1)",
                    "fill-outline-color": "orange"
                }
            }
        };

        const url = new window.URL("/bundle/camera/", window.location.origin);
        url.searchParams.set("source", sourceName);

        ["halo", "apex", "frustum"].forEach((name, i) => {
            const layer = layers[name];
            layer.filter = ["==", "name", name];
            layer.type = "fill";
            layer.layout = {};
            layer.source = sourceName;
            url.hash = `#${name}`;
            layer.id = url.toString().slice(window.location.origin.length);
            el.addLayer(layer, 100+i);
        });
        logger.debug(JSON.stringify(layers));
        // return Object.values(layers);

        let dragging;

        const move = e => {
            if (dragging.layerId.endsWith("frustum")) {
                const o = dragging.point;
                const n = e.point;
                dragging.point = n;
                const c = el.project(this.lonlat);
                this.euler.alpha += Math.atan2(o.y - c.y, o.x - c.x) -
                    Math.atan2(n.y - c.y, n.x - c.x);
            }
            else if (dragging.layerId.endsWith("apex")) {
                const o = dragging.lngLat;
                const n = e.lngLat;
                dragging.lngLat = n;
                geolocation.simulateMoveTo(
                    this.lonlat[0] + n.lng - o.lng, this.lonlat[1] + n.lat - o.lat
                );
                // this.lonlat[0] += n.lng-o.lng;
                // this.lonlat[1] += n.lat-o.lat;
            }
            // logger.debug(dragging.layerId, this.lonlat, this.euler.alpha);
            this.draw();
        };

        const up = e => {
            el.off("mousemove", move);
            el.off("touchmove", move);
            el.cursor = "move";
        };

        /** @todo roate cursor? stackoverflow.com/questions/8017727 */
        // Object.values(layers).forEach(layer => {
        ["apex", "frustum"].forEach(name => {
            const layer = layers[name];
            el.on("mouseenter", layer.id, e => el.cursor = "move");
            el.on("mouseleave", layer.id, e => el.cursor = "");
            el.on("mousedown", layer.id, e => {
                e.preventDefault();
                // logger.debug('mousedown', layer.id);
                dragging = { layerId: layer.id, lngLat: e.lngLat, point: e.point };
                el.on("mousemove", move);
                el.once("mouseup", up);
                el.cursor = "grab";
            });
            el.on("touchstart", layer.id, e => {
                if (e.points.length !== 1) return;
                e.preventDefault();
                // logger.debug('touchstart', layer.id);
                dragging = { layerId: layer.id, lngLat: e.lngLat, point: e.point };
                el.on("touchmove", move);
                el.once("touchend", up);
                el.cursor = "grab";
            });
        });
    }

    /*
    async setup() {
         const control = this.mapTag.getControl('geolocate');

           if (control)
           this.setupControl(control);
           else
           this.mapTag.one('control-added', (name, control) => {
           if (name === 'geolocate')
           this.setupControl(control);
           });

        // this.frustum = this.component.loadPrefSync('frustum', { near: 2.5, far: 15, aspect: 1.0, fov: Math.PI/5 });

        const hasGeolocation = await geolocation.isAvailable();
        const hasOrientation = await orientation.isAvailable();

        if (!hasGeolocation || !hasOrientation) {
        // TODO: listen for future availability?
            this.component.logger.warn("geolocation or orientation not available");
            return;
        }

        const positionGetter = geolocation.getCurrentPosition();
        const position = await positionGetter.get();
        this.lonlat = [position.coords.longitude, position.coords.latitude];
        this.accuracy = 0;
        this.euler = { alpha: 0 };

        const source = { type: "geojson", data: this.getGeojson() };
        await this.mapTag.addSource(sourceName, source);

        const map = this.mapTag.getMap();

        this.draw = () => el.getSource(sourceName).setData(this.getGeojson());
        // el.easeTo({ center: this.lonlat });
        // el.easeTo({ bearing: toDegrees(-this.euler.alpha) });

        geolocation.on("position", _position => {
            this.lonlat = [_position.coords.longitude, _position.coords.latitude];
            this.accuracy = _position.accuracy;
            this.draw();
        });
        geolocation.enableTracking(true);

        orientation.on("orientation", _orientation => {
            this.euler = _orientation.euler;
            this.draw();
        });
        await orientation.enableReading(10);

        window.addEventListener("orientationchange", () => this.draw(), false);

        // el.on('pointermove', e => {
        // });
        // el.on('pointerup', e => dragging.feature = null);

        // const videoControl = new CheckBoxControl(this.component, 'video', enabled => {
        //     this.trigger('video', enabled);
        //     const op = enabled ? 'add' : 'remove';
        //     positionSource[op+'Feature'](frustumFeature);
        // });
        // el.addControl(videoControl);
    }
    */

    setFrustum(frustum) {
        this.frustum = frustum;
        this.savePrefSync("frustum", frustum);
    }

    setupControl(control) {
        control.on("trackuserlocationstart", () => logger.debug("trackuserlocationstart"));
        control.on("geolocate", () => logger.debug("geolocate"));// this.setPosition);
        control.on("trackuserlocationend", () => logger.debug("trackuserlocationend"));
        control.on("error", console.error.bind(console));
    }

    getView() {
        return {
            lonlat: this.lonlat,
            euler: this.euler,
            frustum: this.frustum
        };
    }
}

window.customElements.define(tagName, CameraMapletElement);
