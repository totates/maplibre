import { MaplibreLayerBaseElement } from "external:@totates/maplibre-gl/v1:base";

import { tagName } from "./index.json";

class MaplibrePaintElement extends MaplibreLayerBaseElement {
    static get attributeTypes() {
        return {
            "line-color": "str",
            "line-width": "number"
        };
    }
}

window.customElements.define(tagName, MaplibrePaintElement);
