import logging from "external:@totates/logging/v1";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";

const logger = logging.getLogger("@totates/maplibre-gl/v1:base");

export class MaplibreLayerBaseElement extends QuotquotElement {
    async getData() {
        const type = this.constructor.attributeTypes;
        const data = {};
        for (const attr of this.attributes) {
            if (attr.name.startsWith("data-")) {
                const name = attr.name.slice(5);
                let value = attr.value;
                if (name in type) {
                    switch (type[name]) {
                        case "number":
                            value = parseFloat(value);
                            break;
                        case "str":
                            break;
                        default:
                            logger.warn(`unhandled type "${type[name]}" for "${name}"`);
                    }
                }
                else
                    logger.warn(`unmanaged attribute "${name}"`);
                data[name] = value;
            }
        }
        return data;
    }
}
