import logging from "external:@totates/logging/v1";

// import turf from 'turf';

// import maplibregl from 'maplibre-gl';///dist/maplibre-gl-csp.js';
// import * as spec from 'maplibre-gl/src/style-spec';

const logger = logging.getLogger("element-maplibre");
logger.setLevel("DEBUG");

/* https://stackoverflow.com/questions/33838802/mapbox-gl-change-icon-color
 * https://stackoverflow.com/questions/63299999/how-can-i-create-sdf-icons-used-in-mapbox-from-png
 * https://github.com/nickpeihl/maki-sdf-sprites/blob/master/src/index.js
 * https://github.com/mapbox/maki/blob/v0.5.0/sdf-render.js
 * https://github.com/dy/bitmap-sdf
 * https://github.com/dy/svg-path-sdf
 * https://github.com/mapbox/spritezero
 * https://github.com/mapbox/mapbox-gl-js/issues/3605 (Support icon-color for non-SDF icons)
 * https://github.com/fxi/svgToSprite
 * https://docs.mapbox.com/mapbox-gl-js/example/add-image-generated/
 */
/*
  import pathSdf from 'svg-path-sdf';
  function path2sdf(path, w, h) {
  const sdf = pathSdf(path);
  const arr = new Uint8ClampedArray(w*h*4);
  for (let i = 0; i < w; ++i) {
  for (let j = 0; j < h; ++j) {
  const k = j*w+i;
  const n = k*4;
  arr[n] = arr[n+1] = arr[n+2] = sdf[k]*255;
  arr[n+3] = 255;
  }
  }
  return new window.ImageData(arr, w, h);
  }
*/

// import LanguageControl from 'maplibre-gl-controls/lib/language';
// maplibregl.workerUrl = "https://api.maplibre.com/maplibre-gl-js/v1.10.1/maplibre-gl-csp-worker.js";

// await this.useStylesheet(`style.maplibre`);
// export default { maplibregl, spec };
// import Map from 'maplibre-gl/src/ui/map.js';

/* export function createFilter(...args) {
   return spec.featureFilter(...args);
   }*/

// https://github.com/spatialdev/PGRestAPI/blob/master/docs/VectorTiles.md
// https://en.wikipedia.org/wiki/Vector_tiles

// const wildcardToRegExp = s => new RegExp('^' + s.split(/\*+/).map(p => p.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&')).join('.*') + '$');


export class BaseControl {
    constructor(name) {
        this.name = name;
    }

    onAdd(map) {
        this.map = map;
        this.container = document.createElement("div");
        this.container.className = "maplibregl-ctrl maplibregl-ctrl-group";
        // this.container.textContent = 'My custom control';

        const button = document.createElement("button");
        button.className = "maplibregl-ctrl-icon";
        button.classList.add(`maplibregl-ctrl-${this.name}`);
        button.onclick = e => {
            e.stopPropagation();
            const active = `maplibregl-ctrl-${this.name}-active`;
            const enabled = button.classList.contains(active);
            if (enabled) {
                this.onDisabled();
                button.classList.remove(active);
            }
            else {
                this.onEnabled();
                button.classList.add(active);
            }
        };

        this.container.appendChild(button);
        return this.container;
    }

    onRemove() {
        this.container.parentNode.removeChild(this.container);
        this.map = null;
    }
}

export class FocusControl extends BaseControl {
    constructor(name, tag) {
        super(name);
        this.tag = tag;
    }

    onEnabled() {
        this.tag.show();
    }

    onDisabled() {
        this.tag.hide();
    }
}
